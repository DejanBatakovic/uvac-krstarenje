$(document).ready(function() {
    
    /* For the sticky navigation */
    $('.js--section-features').waypoint(function(direction) {
        if (direction == "down") {
            $('nav').addClass('sticky');
        } else {
            $('nav').removeClass('sticky');
        }
    }, {
            offset: '50px'
        });
    
    /* Navigation scroll */
    $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });
    
    /* Mobile nav */
    $('.js--nav-icon').click(function() {
        var nav = $('.js--main-nav');
        var icon = $('.js--nav-icon i');
        nav.slideToggle(200);
        if(icon.hasClass('ion-navicon-round')) {
            icon.addClass('ion-close-round');
            icon.removeClass('ion-navicon-round');
        } else {
            icon.addClass('ion-navicon-round');
            icon.removeClass('ion-close-round');
        }
    });
    
    /* Map */
    var map = new GMaps({
        div: '.map',
        zoom: 11,
        lat: 43.447328,
        lng: 19.924034
    });
    
    map.drawRoute({
        origin: [43.459133, 19.831877],
        destination: [43.418484, 19.925980],
        travelMode: 'driving',
        strokeColor: '#333',
        strokeOpacity: 0.6,
        strokeWeight: 6
    });
    
    map.drawRoute({
        origin: [43.426814, 19.830239],
        destination: [43.418484, 19.925980],
        travelMode: 'driving',
        strokeColor: '#333',
        strokeOpacity: 0.6,
        strokeWeight: 6
    });
    
    map.drawRoute({
        origin: [43.438973, 20.077775],
        destination: [43.418484, 19.925980],
        travelMode: 'driving',
        strokeColor: '#333',
        strokeOpacity: 0.6,
        strokeWeight: 6
    });
    
    map.addMarker({
        lat: 43.418484,
        lng: 19.925980,
        title: 'Uvac',
        infoWindow: {
            content: '<p>Mesto polaska</p>'
        }
    });
    
});





























