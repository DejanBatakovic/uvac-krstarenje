<!DOCTYPE html>
<html lang="sr">
    
    <head>
        
        <meta charset="utf-8">
        <link rel="icon" type="image/png" href="../resources/img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Prevoz turista Uvackim jezerom">
        <meta name="keywords" content="Uvac,Uvcem,krstarenje,voznja,jezero,Uvacko,Sjenicko,beloglavi,sup,meandri,vidikovac,Ledena,pecina">
        <meta name="author" content="Dejan Batakovic">
        
        <link rel="stylesheet" type="text/css" href="../vendors/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="../vendors/css/grid.css">
        <link rel="stylesheet" type="text/css" href="../vendors/css/ionicons.min.css">
        <link rel="stylesheet" type="text/css" href="../resources/css/style.css">
        <link rel="stylesheet" type="text/css" href="../resources/css/queries.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400&amp;subset=latin-ext" rel="stylesheet">
        <title>Uvac krstarenje</title>
        
    </head>
    
    <body>
        <header id="top">
            <nav>
                <div class="row">
                    <img src="../resources/img/logo.png" alt="Logo" class="logo">
                    <a href="#top"><img src="../resources/img/logo.png" alt="Logo" class="logo-small"></a>
                    <ul class="main-nav js--main-nav">
                        <li><a href="#program">Program</a></li>
                        <li><a href="#uvac">Uvac</a></li>
                        <li><a href="#form-section">Contact</a></li>
                        <li><a href="#lokacija">Location</a></li>
                        <li class="language-link"><a href="/">SR <img src="../resources/img/rs.png" alt=""></a></li>
                    </ul>
                    <a class="language-link-mob" href="/"><img src="../resources/img/rs.png" alt=""></a>
                    <a class="mobile-nav-icon js--nav-icon"><i class="ion-navicon-round"></i></a>
                </div>
            </nav>
            <div class="hero-text-box">
                <h1>CANYON UVAC CRUISE</h1>
                <h2>Experience the unforgettable adventure!</h2>
            </div>
        </header>
        
        <section class="section-features js--section-features" id="program">
            <div class="row">
                <h2>TOUR PROGRAM</h2>
            </div>
            <div class="row js--wp-1">
                <div class="col span-1-of-4 box">
                    <i class="ion-flag icon-big"></i>
                    <h3>DEPARTURE</h3>
                    <p>
                        Departure every day at 10:30 and 15:15. Meeting point is the Uvac lake dam. The tour lasts about 4 hours.
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-android-boat icon-big"></i>
                    <h3>CRUISE</h3>
                    <p>
                        Cruise Uvac lake and Uvac meanders with length of 16km in one direction. You'll disembark from the boat to visit the Ice Cave and experience walking tour to the sightseeing platform. During this tour, You will have the opportunity to see the Griffon vulture - the king of this sky!
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-ios-paw-outline icon-big"></i>
                    <h3>THE ICE CAVE</h3>
                    <p>
                        Below the steep limestone cliffs of the Uvac canyon, there is the entrance to one of the most attractive caves in Serbia - Ice cave. This cave in the Uvac canyon is one of the most beautiful caves in Serbia, which makes her Serbia’s hidden jewel because it can be reached only by boat through Uvac Lake.
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-images icon-big"></i>
                    <h3>View point</h3>
                    <p>
                        For all adventure lovers, a walking tour to the view point is a real challenge. Walking toursets it’s place on a 1300 m longpath. From the view point, You’ll have a panoramic view of the curving meanders of the river Uvac.
                    </p>
                </div>
            </div>
            <div class="row rez">
                <p>&#8727;For reservations and additional information, please call the phone number at the bottom of the page, or fill in the booking form.</p>
            </div>
        </section>
        
        <section class="section-photos">
            <ul class="photos-showcase clearfix">
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/1.jpg" alt="Jezero">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/2.jpg" alt="Beloglavi sup">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/3.jpg" alt="Brodic">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/4.jpg" alt="Vidikovac brana">
                    </figure>
                </li>
            </ul>
            <ul class="photos-showcase clearfix">
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/5.jpg" alt="Vidikovac">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/6.jpg" alt="Vidikovac 2">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/7.jpg" alt="Pecina">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="../resources/img/8.jpg" alt="Meandri">
                    </figure>
                </li>
            </ul>
        </section>
        
        <section class="section-uvac" id="uvac">
            <div class="row">
                <h2>Uvac</h2> 
            </div>
            <div class="row marg">
                <div class="col span-1-of-2 uvac-box">
                    <p>Special Nature Reserve "Uvac" is a natural good of exceptional importance, i.e. natural goodness of the category I. The central morphological unit of the reserve is the canyon valley of the river Uvac with its tributaries. The waters of Uvac were deeply tapering their trough into the limestone walls and formed a narrow cliff-canyon valley with tall, steep limestone cliffs. The special value of the canyon are meanders, whose capes have a relative height of up to 100m. Everyone who visits this special nature reserve rightly calls it Serbian Colorado.</p>
                </div>
                <div class="col span-1-of-2 uvac-box">
                    <p>"Uvac" was placed under protection in order to preserve the Griffon vulture– rear species of  vulture eagle of the imposing size, with wing span and up to 3m, making it a powerful flyer whose flight was researched by scientists – aeronautics, and applied those research materials in the construction of aircrafts. Griffon vulture is a bird that doesn’t kill, doesn’t catch a preyor seek a victim. Its role in the food chain in the ecosystem is unique and irreplaceable –it is fed exclusively to dead animals, thereby preventing the spread of the disease and doing "natural recycling". That's why this species is appreciated all over the world.</p>
                </div>
            </div>
        </section>
        
        <section class="section-utisci" id="utisci">
            <div class="row">
                <h2>REVIEWS</h2>
            </div>
            <div class="row">
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        Beauty written in the stone, in fantastic meanders of unrealistic beautiful river. And people, serbian people, hospitable, friendly and always there to help a visitor. Well done! 
                        <cite>Family Rankov</cite>
                    </blockquote>
                </div>
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        We are here for the first time, but definitely not the last one. Everything is perfect, nature, river, cave and people –tour guides, without them this tour wouldn’t give a complete experience like this. Thank you!
                        <cite>Family Marić</cite>
                    </blockquote>
                </div>
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        Oh Serbia, you are so beautiful, very beautiful! We enjoyed today at Uvac Lake with good company and our tour guides Kaja and Dejan. Wish you all the best in future work!
                        <cite>Zorica and Feđa</cite>
                    </blockquote>
                </div>
            </div>
        </section>
        
        <section class="section-form" id="form-section">
            <div class="row">
                <h2>CONTACT</h2>
            </div>
            <div class="row">
                <form method="post" action="mailer.php" class="contact-form">
                    <div class="row">
                        
                        <?php
                        if (isset($_GET['success']) && $_GET['success'] == 1)
                            echo "<div class=\"form-messages success\">Vaša poruka je poslata!</div>";
                        else if (isset($_GET['success']) == -1)
                            echo "<div class=\"form-messages error\">Vaša poruka nije poslata! Proverite da li ste dobro uneli podatke.</div>";
                        ?>
                        
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="name">Name</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="text" name="name" id="name" placeholder="Name" required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="email">Email</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="email" name="email" id="email" placeholder="Email" required> 
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="phone">Phone number</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="phone" name="phone" id="phone" placeholder="Phone number" required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label>Message</label>
                        </div>
                        <div class="col span-2-of-3">
                            <textarea name="message" placeholder="Message"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3 mar1">
                            <label>&nbsp;</label>
                        </div>
                        <div class="col span-2-of-3 mar1">
                            <input type="submit" value="Pošalji"> 
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
        <section id="lokacija">
            <div class="map"></div>
        </section>
        
        <footer>
            <div class="row kont">
                <div class="col span-1-of-3 kontakt-telefon">
                    <p>Phone:</p>
                    <p>Katarina Vitorovic<br>+381 69660801</p>
                </div>
                <div class="col span-1-of-3">
                    <p>Email:</p>
                    <p>uvackrstarenje@gmail.com</p>
                </div>
                <div class="col span-1-of-3">
                    <ul class="social-links">
                        <li><a href="https://www.facebook.com/uvackrstarenje/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/uvackrstarenje.rs/?hl=sr" target="_blank"><i class="ion-social-instagram"></i></a>
                        <li><img src="../resources/img/logo.png" alt="Logo" class="logo-foother"></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <p>
                    Copyright &copy; Uvac krstarenje 2018
                </p>
            </div>
        </footer>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="../vendors/js/jquery.waypoints.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyAMfRwbS-PktOcSrXJMuMG0tqiwT24z34E"></script>
        <script src="../resources/js/script.js"></script>
        <script src="../vendors/js/gmaps.js"></script>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117573848-1"></script>
        <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-117573848-1');
        </script>

    </body>
    
</html>