<!DOCTYPE html>
<html lang="sr">
    
    <head>
        
        <meta charset="utf-8">
        <link rel="icon" type="image/png" href="resources/img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Prevoz turista Uvackim jezerom">
        <meta name="keywords" content="Uvac,Uvcem,krstarenje,voznja,jezero,Uvacko,Sjenicko,beloglavi,sup,meandri,vidikovac,Ledena,pecina">
        <meta name="author" content="Dejan Batakovic">
        
        <link rel="stylesheet" type="text/css" href="vendors/css/normalize.css">
        <link rel="stylesheet" type="text/css" href="vendors/css/grid.css">
        <link rel="stylesheet" type="text/css" href="vendors/css/ionicons.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/style.css">
        <link rel="stylesheet" type="text/css" href="resources/css/queries.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400&amp;subset=latin-ext" rel="stylesheet">
        <title>Uvac krstarenje</title>
        
    </head>
    
    <body>
        <header id="top">
            <nav>
                <div class="row">
                    <img src="resources/img/logo.png" alt="Logo" class="logo">
                    <a href="#top"><img src="resources/img/logo.png" alt="Logo" class="logo-small"></a>
                    <ul class="main-nav js--main-nav">
                        <li><a href="#program">Program</a></li>
                        <li><a href="#uvac">Uvac</a></li>
                        <li><a href="#form-section">Kontakt</a></li>
                        <li><a href="#lokacija">Lokacija</a></li>
                        <li class="language-link"><a href="/en">EN <img src="/resources/img/uk-flag.png" alt=""></a></li>
                    </ul>
                    <a class="language-link-mob" href="/en"><img src="/resources/img/uk-flag.png" alt=""></a>
                    <a class="mobile-nav-icon js--nav-icon"><i class="ion-navicon-round"></i></a>
                </div>
            </nav>
            <div class="hero-text-box">
                <h1>Krstarenje kanjonom Uvca</h1>
                <h2>Doživite nezaboravno iskustvo!</h2>
            </div>
        </header>
        
        <section class="section-features js--section-features" id="program">
            <div class="row">
                <h2>Program krstarenja</h2>
            </div>
            <div class="row js--wp-1">
                <div class="col span-1-of-4 box">
                    <i class="ion-flag icon-big"></i>
                    <h3>Polazak</h3>
                    <p>
                        Polazak svakog dana u 10:30h i 15:15h. Kreće se sa brane Uvačkog jezera. Izlet traje oko 4 sata.
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-android-boat icon-big"></i>
                    <h3>Krstarenje</h3>
                    <p>
                        Krstarenje Uvačkim jezerom i meandrima Uvca dužinom od 16km u jednom smeru. Iskrcavanje prilikom obilaska Ledene pećine i pešačke ture do vidikovca. Tokom ove ture imaćete priliku da vidite beloglavog supa - kralja ovog neba!
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-ios-paw-outline icon-big"></i>
                    <h3>Ledena pećina</h3>
                    <p>
                        Ispod strmih krečnjačkih litica kanjona Uvca, nalazi se ulaz u jednu od najatraktivnijih pećina Srbije - Ledenu pećinu. Ova pećina u kanjonu Uvca jedna je od najlepših u Srbiji, a pravi je skriveni biser jer se do nje može doći samo čamcem preko Uvačkog jezera.
                    </p>
                </div>
                <div class="col span-1-of-4 box">
                    <i class="ion-images icon-big"></i>
                    <h3>Vidikovac</h3>
                    <p>
                        Za sve ljubitelje avanture, pešačka tura do vidikovca je pravi izazov. Pešači se obeleženom stazom dugom 1300m. Sa vidikovca se pruža panoramski pogled na uklještene meandre kanjonske doline reke Uvac.
                    </p>
                </div>
            </div>
            <div class="row rez">
                <p>&#8727;Za rezervacije i dodatne informacije pozivite telefon sa dna stranice, ili popunite formu za rezervacije.</p>
            </div>
        </section>
        
        <section class="section-photos">
            <ul class="photos-showcase clearfix">
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/1.jpg" alt="Jezero">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/2.jpg" alt="Beloglavi sup">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/3.jpg" alt="Brodic">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/4.jpg" alt="Vidikovac brana">
                    </figure>
                </li>
            </ul>
            <ul class="photos-showcase clearfix">
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/5.jpg" alt="Vidikovac">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/6.jpg" alt="Vidikovac 2">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/7.jpg" alt="Pecina">
                    </figure>
                </li>
                <li>
                    <figure class="uvac-photo">
                        <img src="resources/img/8.jpg" alt="Meandri">
                    </figure>
                </li>
            </ul>
        </section>
        
        <section class="section-uvac" id="uvac">
            <div class="row">
                <h2>Uvac</h2> 
            </div>
            <div class="row marg">
                <div class="col span-1-of-2 uvac-box">
                    <p>Specijalni rezervat prirode "Uvac" je prirodno dobro od izuzetnog značaja, tj. prirodno dobro I kategorije. Centralnu morfološku celinu rezervata predstavlja kanjonska dolina reke Uvac sa pritokama. Vode Uvca su duboko usekle svoje korito u krečnjačke stene i formirale sužene klisurasto - kanjonske doline sa visokim, strmim krečnjačkim liticama. Posebna vrednost kanjonskih delova su uklješteni meandri, čiji rtovi imaju relativnu visinu i do 100m. Svi koji ga posete sa punim pravom ga nazivaju srpski Kolorado.</p>
                </div>
                <div class="col span-1-of-2 uvac-box">
                    <p>"Uvac" je stavljen pod zaštitu da bi se sačuvao beloglavi sup - retka vrsta orla lešinara, impozantne veličine, raspona krila i do 3m što ga čini moćnim letačem čiji let su istraživali i naučnici - aeronautičari i primenjivali pri konstrukciji letelica. Beloglavi sup je ptica koja ne ubija, ne lovi plen, ne traži žrtvu. Njegova uloga u lancu ishrane u ekosistemu je jedinstvena i nezamenljiva - iskljuciva hrana su mu uginule životinje, čime sprečava širenje zaraza i na taj način čini "prirodnu reciklažu". Zato je ovaj ekolog cenjen svuda u svetu.</p>
                </div>
            </div>
        </section>
        
        <section class="section-utisci" id="utisci">
            <div class="row">
                <h2>Utisci posetilaca</h2>
            </div>
            <div class="row">
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        Lepota zapisana u kamenu, u fantastičnim meandrima nestvarno lepe reke. I ljudi, naši ljudi, gostoljubivi, srdačni i spremni na svaku pomoć posetiocu. Svaka vam čast!
                        <cite>Porodica Rankov</cite>
                    </blockquote>
                </div>
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        Prvi put smo ovde, ali ne i poslednji. Sve je savršeno, priroda, reka, pećina i ljudi - vodiči, bez kojih bi ovo bio jedan obilazak bez potpunog doživljaja. Hvala!
                        <cite>Porodica Marić</cite>
                    </blockquote>
                </div>
                <div class="col span-1-of-3 mar">
                    <blockquote>
                        Oj Srbijo ala si lepa, ma šta lepa, prelepa! Uživali smo danas na Uvačkom jezeru uz dobro društvo i vodiče Kaju i Dejana. Svako dobro u daljem radu.
                        <cite>Zorica i Feđa</cite>
                    </blockquote>
                </div>
            </div>
        </section>
        
        <section class="section-form" id="form-section">
            <div class="row">
                <h2>Kontakt</h2>
            </div>
            <div class="row">
                <form method="post" action="mailer.php" class="contact-form">
                    <div class="row">
                        
                        <?php
                        if (isset($_GET['success']) && $_GET['success'] == 1)
                            echo "<div class=\"form-messages success\">Vaša poruka je poslata!</div>";
                        else if (isset($_GET['success']) == -1)
                            echo "<div class=\"form-messages error\">Vaša poruka nije poslata! Proverite da li ste dobro uneli podatke.</div>";
                        ?>
                        
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="name">Ime</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="text" name="name" id="name" placeholder="Ime" required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="email">Email</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="email" name="email" id="email" placeholder="Email" required> 
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label for="phone">Broj telefona</label>
                        </div>
                        <div class="col span-2-of-3">
                            <input type="phone" name="phone" id="phone" placeholder="Broj telefona" required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3">
                            <label>Poruka</label>
                        </div>
                        <div class="col span-2-of-3">
                            <textarea name="message" placeholder="Poruka"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col span-1-of-3 mar1">
                            <label>&nbsp;</label>
                        </div>
                        <div class="col span-2-of-3 mar1">
                            <input type="submit" value="Pošalji"> 
                        </div>
                    </div>
                </form>
            </div>
        </section>
        
        <section id="lokacija">
            <div class="map"></div>
        </section>
        
        <footer>
            <div class="row kont">
                <div class="col span-1-of-3 kontakt-telefon">
                    <p>Kontakt telefoni:</p>
                    <div class="span-1-of-2 tel">
                        <p>Katarina Vitorovic<br>+381 (0)69660801</p>
                    </div>
                    <div class="span-1-of-2 tel">
                        <p>Dejan Vojinovic<br>+381 (0)69685377<br>+381 (0)652100033</p>
                    </div>
                </div>
                <div class="col span-1-of-3">
                    <p>Email:</p>
                    <p>uvackrstarenje@gmail.com</p>
                </div>
                <div class="col span-1-of-3">
                    <ul class="social-links">
                        <li><a href="https://www.facebook.com/uvackrstarenje/" target="_blank"><i class="ion-social-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/uvackrstarenje.rs/?hl=sr" target="_blank"><i class="ion-social-instagram"></i></a></li>
                        <li><img src="resources/img/logo.png" alt="Logo" class="logo-foother"></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <p>
                    Copyright &copy; Uvac krstarenje 2018
                </p>
            </div>
        </footer>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="vendors/js/jquery.waypoints.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyAMfRwbS-PktOcSrXJMuMG0tqiwT24z34E"></script>
        <script src="resources/js/script.js"></script>
        <script src="vendors/js/gmaps.js"></script>
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117573848-1"></script>
        <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', 'UA-117573848-1');
        </script>

    </body>
    
</html>